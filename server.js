var express = require('express');
var request = require('request');
var router = express();
var cors = require('cors');
const firebase = require('firebase');

var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(cors());
router.use(express.json());

// Initialize Firebase & store config info to a const
var firebase_app = firebase.initializeApp({
      apiKey: "AIzaSyDliGmrsDx2h-jEVh841CGzKxxAnvAQaPk",
      authDomain: "assignment1-8525e.firebaseapp.com",
      databaseURL: "https://assignment1-8525e.firebaseio.com",
      projectId: "assignment1-8525e",
      storageBucket: "assignment1-8525e.appspot.com",
      messagingSenderId: "22249652018"
  });

// Get database reference
var weatherRef = firebase_app.database().ref("weathers");
var usersRef= firebase.database().ref().child("registers");

/* set up the base URL */
const url = 'http://api.openweathermap.org/data/2.5/weather?';

// Home page handles
router.get('/', function(req, res) {
    res.status(200).json({
        Guide: 'For user info(./users); For weather info(./weathers)'
    });
});

// Users handles
router.get('/users', function(req, res) {
  usersRef.once('value').then(function(snap) {
    res.status(200).json(snap.val());
  });
});

router.post('/users', function(req, res) {
    usersRef.once('value', function(snap) {
        firebase_app.database().ref('registers/' + snap.val().length).set(req.body);
        res.status(201).json({
            newUser: req.body,
            message: 'User Added'
        });
    });
});

router.get('/users/:uid', function(req, res) {
    const uid = req.params.uid;
    usersRef.child(uid).once('value').then(function(snap) {
        res.status(200).json(snap.val());
    });
});

router.post('/users/:uid', function(req, res) {
    const uid = req.params.uid;
    var favour = firebase_app.database().ref('registers/' + uid + '/favour/');
    favour.once('value').then(function(snap) {
    var num = snap.val().length;
    if (snap.val()[0][0] == "Empty"){
        favour.child(num - 1).set(req.body);
    } else {
        favour.child(num).set(req.body);
    }
    res.status(201).json({
        message: 'Favour added successfully'
    });
  });
});

router.delete('/users/:uid', function(req, res) {
    const uid = req.params.uid;
    firebase_app.database().ref('registers/' + uid + '/favour/').set([["Empty"]]);
    res.status(201).json({
        message: 'Favour list cleared!'
    });
});



// Weathers handles
router.get('/weathers', function(req, res) {
    weatherRef.once('value').then(function(snap) {
        res.status(200).json(snap.val());
    });
});

router.post('/weathers', (req, res) => {
    firebase_app.database().ref('weathers/' + req.body.city + "").set(req.body);
    res.status(201).json({
        newWeather: req.body,
        message: 'Weather added'
    });
});

router.get('/weathers/:City', function(req, res) {
    const city = req.params.City.toString().trim();
    findCityWeather(city, function(info) {
        res.status(200).json(info);
    });
});

router.patch('/weathers/:City', (req, res) => {
    var city = req.params.City.toString().trim();
    // Get up-to-date weather from openweather
    findCityWeather(city, function(updateWeather) {
        // Replace the old weather data with new data
        firebase_app.database().ref('weathers/' + city).set(updateWeather);
        // If successful, return message
        res.status(200).json({
            updateWeather: updateWeather,
            message: 'Weathers ' + city + ' updated'
        });
    });
});

router.delete('/weathers/:City', (req, res) => {
    const city = req.params.City;
    firebase_app.database().ref('weathers/' + city).set({});
    res.status(201).json({
        message: 'Weather ' + city + ' deleted'
    });
});


function findCityWeather(city, callback) {
    const query_string = { q: city, units: "metric", appid: "44c39f3fa462f86b3fc88f5678e5c5ff" };
    request.get({ url: url, qs: query_string }, function(err, res, body) {
        if (err) {
            console.log(err);
        }
        else {
            const json = JSON.parse(body);
            var newWeather = {};
            if (newWeather[json.name] == undefined) {
                newWeather[json.name] = [];
            }
            newWeather = {
                temp: json.main.temp,
                min: json.main.temp_min,
                max: json.main.temp_max,
                main: json.weather[0].main,
                pic: json.weather[0].icon,
                pre: json.main.pressure,
                hum: json.main.humidity
            };
            callback(newWeather);
        }
    });
}

const http = require('http');
var server = http.createServer(router);
server.listen(process.env.PORT || 3000);